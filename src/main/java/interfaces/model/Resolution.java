package interfaces.model;

public interface Resolution {
    int getWidth();
    int getHeight();
}
