package interfaces.environment;

import interfaces.browser.Browser;

import java.util.List;

public interface Environment {
    void init();
    List<EnvironmentConfig> getEnvironmentConfig();
    List<Browser> getUsedBrowsers();
    void close();
    Browser searchBrowser(String name);
    void setUsedBrowser(Browser browser);
}
