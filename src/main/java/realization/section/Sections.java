package realization.section;


import java.util.List;

public class Sections {
    private List<Section> sections;

    public Sections(List<Section> list) {
        this.sections = list;
    }

    public Section getSectionByName(String name){

        for (Section section : sections){
            if (name.equals(section.getName())){
                return section;
            }
        }

        return null;
    }
}
