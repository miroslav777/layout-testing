package realization.model;

import com.google.gson.annotations.SerializedName;
import interfaces.model.Resolution;

public class DesktopResolution implements Resolution {
    @SerializedName("width")
    private int width;

    @SerializedName("height")
    private int height;

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }
}
