package interfaces.services;

public interface ServerLauncher {
    void launch();
}
