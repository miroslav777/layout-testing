package realization.environments;

import interfaces.browser.Browser;
import interfaces.environment.EnvironmentConfig;
import interfaces.environment.MobileEnvironment;
import interfaces.services.ServerLauncher;
import io.selendroid.client.SelendroidDriver;
import io.selendroid.common.SelendroidCapabilities;
import launcher.Main;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.remote.DesiredCapabilities;
import realization.environments.browser.MobileChromeBrowser;
import realization.environments.config.ScreenOrientationEnvironmentConfig;

import java.util.ArrayList;
import java.util.List;

public class SmartphoneAndroidEnvironment implements MobileEnvironment {
    private List<Browser> browsers = new ArrayList<>();
    private List<EnvironmentConfig> environmentConfigs = new ArrayList<>();
    private SelendroidDriver selendroidDriver;
    private String pathToJarFileForRunServer = Main.absolutePathToProject + "/res/drivers/selendroid-standalone-0.17.0-with-dependencies.jar";
    private DesiredCapabilities caps;
    private ServerLauncher serverLauncher;

    public SmartphoneAndroidEnvironment() throws Exception {
        this.caps = SelendroidCapabilities.android();

        this.selendroidDriver = new SelendroidDriver(caps);

        browsers.add(new MobileChromeBrowser(selendroidDriver));


        environmentConfigs.add(new ScreenOrientationEnvironmentConfig(ScreenOrientation.PORTRAIT));
        environmentConfigs.add(new ScreenOrientationEnvironmentConfig(ScreenOrientation.LANDSCAPE));
    }

    @Override
    public List<EnvironmentConfig> getEnvironmentConfig() {
        return environmentConfigs;
    }

    public List<Browser> getUsedBrowsers() {
        return browsers;
    }

    @Override
    public void init() {
        selendroidDriver.rotate(ScreenOrientation.PORTRAIT);
        //serverLauncher = new LaucnherSelendroidServer(pathToJarFileForRunServer);
        serverLauncher.launch();
    }

    @Override
    public void close() {

    }

    @Override
    public Browser searchBrowser(String name) {
        //TODO реализовать в случае использования нескольких браузеров на андроиде
        return null;
    }

    @Override
    public void setUsedBrowser(Browser browser) {
        //TODO реализовать в случае использования нескольких браузеров на андроиде
    }

    @Override
    public void changeScreenOrientation(ScreenOrientation screenOrientation) {
        selendroidDriver.rotate(screenOrientation);
    }

    @Override
    public ScreenOrientation getOrientation() {
        return selendroidDriver.getOrientation();
    }
}
