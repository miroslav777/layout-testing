package interfaces.services;

public interface Service {
    void launch();
}
