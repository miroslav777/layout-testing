package realization.environments.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static launcher.Main.absolutePathToProject;

public class DesktopChromeBrowser extends DesktopBrowser{

    @Override
    protected String setPathToFolderWithResults() {
        return absolutePathToProject +  "/res/results/desktop/chrome/";
    }

    @Override
    protected WebDriver setWebDriver() {
        return new ChromeDriver();
    }

    @Override
    protected String setPathToDriver() {
        return absolutePathToProject + "/res/drivers/chromedriver";
    }

    @Override
    protected String setSystemDriverName() {
        return  "webdriver.chrome.driver";
    }

    @Override
    protected String setName() {
        return "chrome";
    }
}
