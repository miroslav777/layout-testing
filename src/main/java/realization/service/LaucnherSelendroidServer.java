package realization.service;

import interfaces.services.ServerLauncher;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class LaucnherSelendroidServer implements ServerLauncher {
    private final String pathToJarFile ;
    private LaucnherSelendroidServer laucnherSelendroidServer;
    private Thread threadExec ;

    public LaucnherSelendroidServer(String pathToJarFile, LaucnherSelendroidServer laucnherSelendroidServer) {
        this.pathToJarFile = pathToJarFile;
        this.laucnherSelendroidServer = laucnherSelendroidServer;
    }

    @Override
    public void launch() {

        threadExec = new Thread(new Runnable() {
            @Override
            public void run() {
                java.lang.Runtime rt = java.lang.Runtime.getRuntime();
                try {
                    rt.exec("java -jar " + pathToJarFile);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        threadExec.start();

        URL oracle = null;
        try {
            oracle = new URL("http://www.oracle.com/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            URLConnection yc = oracle.openConnection();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
