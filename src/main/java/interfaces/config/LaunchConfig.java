package interfaces.config;


import interfaces.environment.Environment;
import realization.section.Section;

import java.util.List;

public interface LaunchConfig {
    String getBaseUrlWithActualImage();
    String getBaseUrlWithExpectedImage();
    List<Environment> getEnvironments();
    Section getSection();
}
