package realization.environments.browser;

import launcher.Main;
import org.openqa.selenium.WebDriver;

public class MobileChromeBrowser extends MobileBrowser {
    public MobileChromeBrowser(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    protected String setPathToFolderWithResults() {
        return Main.absolutePathToProject + "/res/results/mobile/android/";
    }

    @Override
    protected String setName() {
        return "chrome";
    }
}
