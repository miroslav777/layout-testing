package realization.environments.config;

import interfaces.browser.Browser;
import interfaces.environment.Environment;
import interfaces.environment.EnvironmentConfig;
import interfaces.model.Resolution;
import realization.environments.browser.DesktopBrowser;

public class BrowserSizeEnvironmentConfig implements EnvironmentConfig {
    private Resolution resolution;

    public BrowserSizeEnvironmentConfig(Resolution resolution) {
        this.resolution = resolution;
    }

    @Override
    public void applyConfig(Environment environment) {
        for (Browser browser : environment.getUsedBrowsers()){
            ((DesktopBrowser) browser).setSize(resolution.getWidth(),resolution.getHeight());
        }
    }

    @Override
    public String getName() {
        return resolution.getWidth() + "x" + resolution.getHeight();
    }

}
