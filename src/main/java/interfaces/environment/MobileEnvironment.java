package interfaces.environment;

import org.openqa.selenium.ScreenOrientation;

public interface MobileEnvironment extends Environment {
    void changeScreenOrientation(ScreenOrientation screenOrientation);
    ScreenOrientation getOrientation();
}
