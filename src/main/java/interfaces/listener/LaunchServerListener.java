package interfaces.listener;

public interface LaunchServerListener {
    void started();
    void finished();
}
