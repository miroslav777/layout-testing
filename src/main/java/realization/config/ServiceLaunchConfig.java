package realization.config;

import interfaces.browser.Browser;
import interfaces.config.LaunchConfig;
import interfaces.environment.Environment;
import interfaces.section.LoaderJson;
import org.apache.commons.validator.routines.UrlValidator;
import realization.environments.MacOSEnvironment;
import realization.section.Section;

import java.util.ArrayList;
import java.util.List;

import static launcher.Main.absolutePathToProject;

public class ServiceLaunchConfig implements LaunchConfig{

    private final UrlValidator urlValidator = new UrlValidator();
    private List<Environment> environments = new ArrayList<>(2);
    private String baseUrlWithActualImage;
    private String baseUrlWithExpectedImage;
    private Section section;
    private final LoaderJson loaderJson;
    private final String pathToJsonFile = absolutePathToProject + "/res/config/paths/paths.json";

    public ServiceLaunchConfig(String[] args, LoaderJson loaderJson){
        this.loaderJson = loaderJson;
        initEnvironments(args);
        initBaseUrls(args);
        initSection(args);
    }

    private void initSection(String[] args) {
        String argumentSection = null;
        if (args.length>=3){
            argumentSection = args[2];
        }
        section = loaderJson.uploadPathsFromJsonFile(pathToJsonFile, argumentSection);
    }

    private void initBaseUrls(String[] args) {
        checkLength(args);
        baseUrlWithActualImage = args[0];
        baseUrlWithExpectedImage = args[1];
        checkUrl(baseUrlWithActualImage);
        checkUrl(baseUrlWithExpectedImage);
    }

    private void checkLength(String[] args) {
        if (args.length < 2){
            throw new IllegalArgumentException();
        }
    }

    private void checkUrl(String url){
        if (!urlValidator.isValid(url)){
            throw new IllegalArgumentException(url + "is not valid");
        }
    }

    private void initEnvironments(String[] args) {
        MacOSEnvironment macOSEnvironment = new MacOSEnvironment();
        if (args.length>=4){
            Browser browser = macOSEnvironment.searchBrowser(args[3]);
            if (browser == null){
                throw new NullPointerException();
            }
            macOSEnvironment.setUsedBrowser(browser);
        }
        environments.add(macOSEnvironment);
    }

    @Override
    public String getBaseUrlWithActualImage() {
        return baseUrlWithActualImage;
    }

    @Override
    public String getBaseUrlWithExpectedImage() {
        return baseUrlWithExpectedImage;
    }

    @Override
    public List<Environment> getEnvironments() {
        return environments;
    }

    @Override
    public Section getSection() {
        return section;
    }
}
