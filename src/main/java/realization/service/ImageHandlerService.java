package realization.service;

import interfaces.browser.Browser;
import interfaces.config.LaunchConfig;
import interfaces.environment.Environment;
import interfaces.environment.EnvironmentConfig;
import interfaces.handler.FileHandler;
import interfaces.handler.ImageHandler;
import interfaces.services.Service;
import realization.section.Path;
import ru.yandex.qatools.ashot.Screenshot;

import java.awt.image.BufferedImage;

public class ImageHandlerService implements Service {

    private final LaunchConfig launchConfig;
    private final ImageHandler imageHandler;
    private final FileHandler fileHandler;

    public ImageHandlerService(LaunchConfig launchConfig, ImageHandler imageHandler, FileHandler fileHandler) {
        this.launchConfig = launchConfig;
        this.imageHandler = imageHandler;
        this.fileHandler = fileHandler;
    }

    @Override
    public void launch() {
        for (Environment environment : launchConfig.getEnvironments()){

            environment.init();

            for (EnvironmentConfig environmentConfig: environment.getEnvironmentConfig()){

                //noinspection unchecked
                environmentConfig.applyConfig(environment);

                for (Browser browser : environment.getUsedBrowsers()){

                    for (Path path : launchConfig.getSection().getPaths()) {

                        String baseUrlWithActualImage = launchConfig.getBaseUrlWithActualImage();
                        String baseUrlWithExpectedImage = launchConfig.getBaseUrlWithExpectedImage();

                        /*
                        if (launchConfig.getSection().isRequiredHTTPBasicAuth()){
                            baseUrlWithActualImage = browser.getNewUrlFromBacisAuth(baseUrlWithActualImage,
                                    launchConfig.getSection().getLogin(),
                                    launchConfig.getSection().getPass());
                        }*/

                        Screenshot actualScreenshot = browser.getScreenshot(baseUrlWithActualImage+path.getPath());
                        Screenshot expectedScreenshot = browser.getScreenshot(baseUrlWithExpectedImage+path.getPath());

                        BufferedImage resultImage = imageHandler.improseImages(actualScreenshot, expectedScreenshot);
                        fileHandler.writeImageToFile(browser.getPathToResultFolder(), path.getName() + "_" + environmentConfig.getName(), resultImage);
                    }

                }
            }
            environment.close();
        }
    }
}
