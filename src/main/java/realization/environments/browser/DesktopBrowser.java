package realization.environments.browser;

import interfaces.browser.Browser;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import java.net.MalformedURLException;
import java.net.URL;

public abstract class DesktopBrowser implements Browser {

    protected String pathToResultFolder;
    protected String name;
    protected String pathToDriver;
    protected WebDriver webDriver;
    protected String systemDriverName;

    DesktopBrowser(){
        systemDriverName = setSystemDriverName();
        pathToDriver = setPathToDriver();
        pathToResultFolder = setPathToFolderWithResults();
        name = setName();
        System.setProperty(systemDriverName, pathToDriver);
        webDriver = setWebDriver();
    }

    protected abstract String setPathToFolderWithResults();

    protected abstract WebDriver setWebDriver();

    protected abstract String setPathToDriver();

    protected abstract String setSystemDriverName();

    protected abstract String setName();

    @Override
    public Screenshot getScreenshot(String url) {
        AShot aShot = new AShot();

        aShot.shootingStrategy(ShootingStrategies.viewportPasting(300));

        webDriver.get(url);

        return aShot.takeScreenshot(webDriver);
    }

    @Override
    public String getNewUrlFromBacisAuth(String url, String login, String pass) {
        URL newUrl = null;
        try {
            newUrl = new URL(url);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return newUrl.getProtocol() + "://"+login+":" + pass + "@" + newUrl.getHost();
    }

    @Override
    public String getPathToResultFolder() {
        return pathToResultFolder;
    }

    public void setSize(int width, int height) {
        webDriver.manage().window().setSize(new Dimension(width,height));
    }

    @Override
    public void close() {
        webDriver.close();
    }

    @Override
    public String getName() {
        return name;
    }
}
