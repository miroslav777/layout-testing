package launcher;

import interfaces.config.LaunchConfig;
import interfaces.handler.FileHandler;
import interfaces.handler.ImageHandler;
import realization.config.ServiceLaunchConfig;
import realization.handler.ImproseImageHandler;
import realization.handler.WriteFileHandler;
import realization.loader.PathConfig;
import realization.service.ImageHandlerService;

import java.io.IOException;

public class Main {

    public static String absolutePathToProject = System.getProperty("user.dir");

    public static void main(String[] args) throws IOException {
        LaunchConfig serviceLaunchConfig = new ServiceLaunchConfig(args, new PathConfig());
        FileHandler fileHandler = new WriteFileHandler();
        ImageHandler imageHandler = new ImproseImageHandler();
        new ImageHandlerService(serviceLaunchConfig, imageHandler,fileHandler).launch();
    }
}
