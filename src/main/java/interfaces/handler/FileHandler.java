package interfaces.handler;

import java.awt.image.BufferedImage;

public interface FileHandler {
    void writeImageToFile(String path, String name, BufferedImage bufferedImage);
}
