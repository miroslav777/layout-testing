package realization.environments.browser;

import interfaces.browser.Browser;
import io.selendroid.client.SelendroidDriver;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public abstract class MobileBrowser implements Browser {

    protected String pathToResultFolder;
    protected String name;
    protected WebDriver webDriver;

    MobileBrowser(WebDriver webDriver){
        pathToResultFolder = setPathToFolderWithResults();
        name = setName();
        this.webDriver = webDriver;
    }

    protected abstract String setPathToFolderWithResults();

    protected abstract String setName();

    @Override
    public Screenshot getScreenshot(String url) {
        AShot aShot = new AShot();

        aShot.shootingStrategy(ShootingStrategies.viewportPasting(ShootingStrategies.scaling(2),200));
        //aShot.shootingStrategy(ShootingStrategies.simple());

        webDriver.get(url);

        return aShot.takeScreenshot((SelendroidDriver) webDriver);
    }

    @Override
    public String getPathToResultFolder() {
        return pathToResultFolder;
    }

    @Override
    public void close() {
        webDriver.close();
    }

    @Override
    public String getNewUrlFromBacisAuth(String url, String login, String pass) {
        return null;
    }

    @Override
    public String getName() {
        return name;
    }
}
