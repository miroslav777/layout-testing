package realization.loader;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import interfaces.section.LoaderJson;
import realization.section.Section;
import realization.section.Sections;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;


public class PathConfig implements LoaderJson {
    @Override
    public Section uploadPathsFromJsonFile(String pathToJson, String section) {

        if (section ==null){
            throw new NullPointerException();
        }

        Gson gson = new Gson();
        Sections sections = null;

        try (Reader reader = new FileReader(pathToJson)) {
            List<Section> list = gson.fromJson(reader,new TypeToken<List<Section>>(){}.getType());
            sections = new Sections(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sections.getSectionByName(section);
    }
}
