package realization.environments.config.resolution;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import interfaces.config.ResolutionConfig;
import interfaces.model.Resolution;
import launcher.Main;
import realization.model.DesktopResolution;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

public class DesktopResolutionConfig implements ResolutionConfig{

    private final String pathToResolutions = Main.absolutePathToProject + "/res/config/resolution/resolution.json";

    @Override
    public List<Resolution> resolutions() {

        Gson gson = new Gson();
        List<Resolution> resolutions = null;

        try (Reader reader = new FileReader(pathToResolutions)) {
            resolutions = gson.fromJson(reader,new TypeToken<List<DesktopResolution>>(){}.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resolutions;
    }
}
