package realization.handler;

import interfaces.handler.ImageHandler;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

import java.awt.image.BufferedImage;

public class ImproseImageHandler implements ImageHandler {
    @Override
    public BufferedImage improseImages(Screenshot actualScreenshot, Screenshot expectedScreenshot) {
        ImageDiff diff = new ImageDiffer().makeDiff(actualScreenshot, expectedScreenshot);
        return diff.getMarkedImage();
    }
}
