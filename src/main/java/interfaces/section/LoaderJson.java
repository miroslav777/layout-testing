package interfaces.section;

import realization.section.Section;

public interface LoaderJson {
    Section uploadPathsFromJsonFile(String pathToJson, String section);
}
