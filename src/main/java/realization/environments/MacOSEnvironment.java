package realization.environments;

import interfaces.browser.Browser;
import interfaces.config.ResolutionConfig;
import interfaces.environment.Environment;
import interfaces.environment.EnvironmentConfig;
import interfaces.model.Resolution;
import realization.environments.browser.DesktopChromeBrowser;
import realization.environments.browser.DesktopFirefoxBrowser;
import realization.environments.config.BrowserSizeEnvironmentConfig;
import realization.environments.config.resolution.DesktopResolutionConfig;

import java.util.ArrayList;
import java.util.List;

public class MacOSEnvironment implements Environment {

    private List<Browser> browsers = new ArrayList<>();
    private List<EnvironmentConfig> environmentConfigs = new ArrayList<>();
    private List<Browser> usedBrowsers = new ArrayList<>();
    private ResolutionConfig resolutionConfig = new DesktopResolutionConfig();

    public MacOSEnvironment(){
        browsers.add(new DesktopChromeBrowser());
        browsers.add(new DesktopFirefoxBrowser());

        for (Resolution resolution : resolutionConfig.resolutions()){
            environmentConfigs.add(new BrowserSizeEnvironmentConfig(resolution));
        }
    }

    @Override
    public void init() {

    }

    @Override
    public List<EnvironmentConfig> getEnvironmentConfig() {
        return environmentConfigs;
    }

    public List<Browser> getUsedBrowsers() {
        if (usedBrowsers.isEmpty()){
            return browsers;
        }
        return usedBrowsers;
    }

    @Override
    public void close() {
        for (Browser browser: browsers){
            browser.close();
        }
    }

    @Override
    public Browser searchBrowser(String name) {
        for (Browser browser : browsers){
            if (name.equals(browser.getName())){
                return browser;
            }
        }
        return null;
    }

    @Override
    public void setUsedBrowser(Browser browser) {
        usedBrowsers.add(browser);
    }
}
