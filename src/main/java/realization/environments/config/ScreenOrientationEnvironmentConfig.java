package realization.environments.config;

import org.openqa.selenium.ScreenOrientation;
import interfaces.environment.EnvironmentConfig;
import interfaces.environment.MobileEnvironment;

public class ScreenOrientationEnvironmentConfig implements EnvironmentConfig<MobileEnvironment> {
    private final ScreenOrientation screenOrientation;

    public ScreenOrientationEnvironmentConfig(ScreenOrientation screenOrientation) {
        this.screenOrientation = screenOrientation;
    }

    @Override
    public void applyConfig(MobileEnvironment environment) {
        environment.changeScreenOrientation(screenOrientation);
    }

    @Override
    public String getName() {
        return screenOrientation.value();
    }
}
