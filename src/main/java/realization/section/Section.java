package realization.section;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Section{
    @SerializedName("name")
    private String name;

    @SerializedName("requiredHTTPBacisAuth")
    private boolean requiredHTTPBacisAuth;

    @SerializedName("login")
    private String login;

    @SerializedName("pass")
    private String pass;

    @SerializedName("section")
    private List<Path> paths;

    private boolean isAuth;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAuth() {
        return isAuth;
    }

    public void setAuth(boolean auth) {
        this.isAuth = auth;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public List<Path> getPaths() {
        return paths;
    }

    public void setPaths(List<Path> paths) {
        this.paths = paths;
    }

    public boolean isRequiredHTTPBasicAuth() {
        return requiredHTTPBacisAuth;
    }

    public void setRequiredAuth(boolean requiredHTTPBacisAuth) {
        this.requiredHTTPBacisAuth = requiredHTTPBacisAuth;
    }

}
