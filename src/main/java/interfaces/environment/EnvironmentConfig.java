package interfaces.environment;

public interface EnvironmentConfig<T extends Environment> {
   void applyConfig(T environment);
   String getName();
}
