package realization.environments.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static launcher.Main.absolutePathToProject;


public class DesktopFirefoxBrowser extends DesktopBrowser {
    @Override
    protected String setPathToFolderWithResults() {
        return absolutePathToProject +  "/res/results/desktop/firefox/";
    }

    @Override
    protected WebDriver setWebDriver() {
        return new FirefoxDriver();
    }

    @Override
    protected String setPathToDriver() {
        return absolutePathToProject + "/res/drivers/geckodriver";
    }

    @Override
    protected String setSystemDriverName() {
        return "webdriver.gecko.driver";
    }

    @Override
    protected String setName() {
        return "firefox";
    }
}
