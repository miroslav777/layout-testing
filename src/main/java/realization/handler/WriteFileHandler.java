package realization.handler;

import interfaces.handler.FileHandler;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class WriteFileHandler implements FileHandler {

    private final String format = "jpg";

    @Override
    public void writeImageToFile(String path, String name, BufferedImage bufferedImage) {
        try {
            ImageIO.write(bufferedImage, format , new File(path + name + "." + format));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(path + name + " failed");
        }
        System.out.println(path + name + " success");
    }
}
