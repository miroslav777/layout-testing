package realization.section;


import com.google.gson.annotations.SerializedName;

public class Path {
    @SerializedName("name")
    private String name;
    @SerializedName("path")
    private String path;

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
