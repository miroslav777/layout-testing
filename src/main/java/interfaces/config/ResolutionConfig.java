package interfaces.config;

import interfaces.model.Resolution;
import java.util.List;

public interface ResolutionConfig {
    List<Resolution> resolutions();
}
