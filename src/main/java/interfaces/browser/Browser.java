package interfaces.browser;

import ru.yandex.qatools.ashot.Screenshot;

public interface Browser {
    Screenshot getScreenshot(String url);
    String getPathToResultFolder();
    void close();
    String getName();
    String getNewUrlFromBacisAuth(String url, String login, String pass);
}
