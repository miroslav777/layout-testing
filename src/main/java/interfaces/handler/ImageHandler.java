package interfaces.handler;


import ru.yandex.qatools.ashot.Screenshot;

import java.awt.image.BufferedImage;

public interface ImageHandler {
    BufferedImage improseImages(Screenshot actualScreenshot, Screenshot expectedScreenshot);

}
